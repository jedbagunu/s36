const Task = require('../models/Task.js')

module.exports.createTask= (data) =>{
	let newTask = new Task({
		name: data.name
	})
	return newTask.save().then((savedTask, error)=>{
		if(error){
			console.log(error)
			return error
		}
		return savedTask
	})
}

module.exports.getAllTasks = () => {
	return Task.find({}).then((result)=>{
		return result
	})
}

module.exports.updateTask =(task_id, new_data)=> {
	return Task.findById(task_id).then((result, error)=>{
		if (error){
			console.log(error)
			return error
		}
		result.name = new_data.name
		return result.save().then((updatedTask, error)=>{
			if (error) {
				console.log(error)
				return error

			}
			return updatedTask
		})

	})
}
module.exports.deleteTask = (task_id, deleteData)=>{
	return Task.findById(task_id).then((result,error)=>{
		if (error) {
			console.log(error)
			return error
		} 
		result.name = deleteData.name
		return result.deleteOne().then((data, error)=>{
			if (error) {
				console.log(error)
				return error 
			}
				return data
		})
	})
}

//get a task

module.exports.getTask =(task_id, new_data)=> {
	return Task.findById(task_id).then((result, error)=>{
		if (error){
			console.log(error)
			return error
		}
		
		return result.save().then((updatedTask, error)=>{
			if (error) {
				console.log(error)
				return error

			}
			return updatedTask
		})

	})
}

module.exports.updateStatus =(task_id, new_data)=> {
	return Task.findById(task_id).then((result, error)=>{
		if (error){
			console.log(error)
			return error
		}
		result.status = new_data.status
		return result.save().then((updatedStatus, error)=>{
			if (error) {
				console.log(error)
				return error

			}
			return updatedStatus
		})

	})
}



