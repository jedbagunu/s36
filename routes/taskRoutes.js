//imports
const express = require('express')
const router = express.Router()
const TaskController = require('../controllers/TaskController')

//create single task
router.post('/create',(request, response)=>{
	TaskController.createTask(request.body).then((result)=>{
		response.send(result)
	})
})

//get all tasks
router.get('/',(request, response)=>{
	TaskController.getAllTasks().then((result)=>{
		response.send(result)
	})
})

//upadate a task
router.patch('/:id/update', (request, response)=>{
	TaskController.updateTask(request.params.id, request.body).then((result)=>{
		response.send(result)
	})
})

//delete task

/*router.delete('/:id/delete',(request, response)=> {
	TaskController.deleteTask(request.params.id, request.body).then((result)=>{
		response.send(result)
	})
})*/

//Get specific task
router.get('/:id/tasks', (request, response)=>{
	TaskController.getTask(request.params.id, request.body).then((result)=>{
		response.send(result)
	})
})


//Status updated
router.patch('/:id/complete', (request, response)=>{
	TaskController.updateStatus(request.params.id, request.body).then((result)=>{
		response.send(result)
	})
})




//to be import by other files
module.exports = router
