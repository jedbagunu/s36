//for schemas

const mongoose = require('mongoose')

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		status: 'Pending'
	}
})
module.exports = mongoose.model('Task', taskSchema)