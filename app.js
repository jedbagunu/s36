//import modules
const express = require('express')
const mongoose = require ('mongoose')
const dotenv = require('dotenv')
const taskRoutes = require('./routes/taskRoutes')

//initialize dotenv
dotenv.config()



//server setup
const app = express()
const port = 3003
app.use(express.json())
app.use(express.urlencoded({extended: true}))

//mongoDB connection
mongoose.connect(`mongodb+srv://JEDBAGUNU:${process.env.MONGODB_PASSWORD}@cluster0.bxtf324.mongodb.net/S36-todo?retryWrites=true&w=majority`,{
	useNewUrlParser: true,
	useUnifiedTopology: true
})


//connections
let db = mongoose.connection
db.on('error',()=>console.error('Connection error'))
db.on('open',()=>console.log('Connected to MongoDB!'))

//routes handling
app.use('/tasks',taskRoutes)

//server listening
app.listen(port, ()=> console.log(`Server running at localhost : ${port}`))